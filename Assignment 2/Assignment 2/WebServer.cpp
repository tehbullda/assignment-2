#include "WebServer.h"
#include <iostream>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
#include <sstream>
#include <fstream>

#define MaxAllowedTime 300000
#define BufferSize 4096

WebServer::WebServer() {
	m_local = nullptr;
}


WebServer::~WebServer() {
	WSACleanup();
	if (m_local) {
		closesocket(m_local->m_sock);
		delete m_local;
		m_local = nullptr;
	}

	for (unsigned i = 0; i < m_clientlist.size(); i++) {
		closesocket(m_clientlist[i]->m_sock);
		delete m_clientlist[i];
	}
}

bool WebServer::Initialize() {
	if (!Load_page("index.html")) {
		return false;
	}
	UpdateHeader();
	m_local = new Client;
	if (WSAStartup(MAKEWORD(2, 1), &m_data) != 0) {
		return false;
	}
	m_local->m_addr.sin_family = AF_INET;
	m_local->m_addr.sin_addr.S_un.S_addr = INADDR_ANY;
	m_local->m_addr.sin_port = htons(9090);
	memset(m_local->m_addr.sin_zero, 0, sizeof(m_local->m_addr.sin_zero));
	m_local->m_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	m_local->m_timestamp = timeGetTime();

	if (bind(m_local->m_sock, (const sockaddr*)&m_local->m_addr, sizeof(struct sockaddr_in)) != 0) {
		std::cout << WSAGetLastError() << std::endl;
		return false;
	}
	if (listen(m_local->m_sock, SOMAXCONN) != 0) {
		return false;
	}
	m_timeval.tv_sec = 0;
	m_timeval.tv_usec = 10;
	std::cout << "Server running." << std::endl;
	return true;
}

bool WebServer::Run() {
	m_fdset.fd_count = 1;
	m_fdset.fd_array[0] = m_local->m_sock;
	int result = select(1, &m_fdset, 0, 0, &m_timeval);
	if (result > 0) {
		SOCKET tempsock;
		struct sockaddr_in tempaddr;
		int size = sizeof(sockaddr_in);
		tempsock = accept(m_local->m_sock, (sockaddr*)&tempaddr, &size);
		if (tempsock == INVALID_SOCKET) {
			std::cout << "Socket error(accept): " << WSAGetLastError() << std::endl;
		}
		else {
			Add_client(tempsock, tempaddr);
			m_local->m_timestamp = timeGetTime(); //Someone did something, so reset the servers deathtimer.
		}
	}
	for (unsigned i = 0; i < m_clientlist.size(); i++) {
		if (Client_has_data(*m_clientlist[i])) {
			m_local->m_timestamp = timeGetTime(); //Someone did something, so reset the servers deathtimer.
			Receive_data_from_client(m_clientlist[i]);
		}
		if (timeGetTime() - m_clientlist[i]->m_timestamp > MaxAllowedTime || !m_clientlist[i]->m_keepalive) {
			RemoveClient(m_clientlist[i], i);
		}
	}
	if (timeGetTime() - m_local->m_timestamp > MaxAllowedTime && m_clientlist.size() == 0) {
		return false; // If nothing has happened for 5 minutes and no clients are connected we close the server.
	}
	return true;
}

bool WebServer::Client_has_data(Client client) {
	unsigned long ret;
	ioctlsocket(client.m_sock, FIONREAD, &ret);
	return ret > 0;
}

void WebServer::Receive_data_from_client(Client *client) {
	char buffer[BufferSize] = { 0 };
	int bytes = recv(client->m_sock, buffer, sizeof(buffer) - 1, 0);
	if (bytes < 0) {
		std::cout << "Socket error(receive): " << WSAGetLastError() << std::endl;
	}
	else if (bytes == 0) {
	}
	else {
		std::cout << buffer << std::endl;
		std::string str = buffer;
		if (str.find("Connection: close") != std::string::npos) {
			client->m_keepalive = false;
		}
		if (str.find("GET") != std::string::npos) {
			if (str.find("/../") != std::string::npos) {
				Print_client_address(&client->m_addr);
				std::cout << " Attempting to access subfolders (GET /..)" << std::endl;
				Load_page("forbidden.html");
				UpdateHeader(403);
			}
			else if (!Load_page(str.substr(str.find("/"), (str.find("HTTP") - 1) - str.find("/")))) {
				Load_page("404.html");
				UpdateHeader(404);
			}
			else {
				UpdateHeader();
			}
			std::string msg = m_header + m_body;
			send(client->m_sock, msg.c_str(), msg.length(), 0);
		}
	}
}

bool WebServer::Load_page(const std::string &filename, const std::string &directory) {
	std::ifstream stream;
	std::stringstream ss;
	std::string line;
	m_body = "";
	if (filename == "/") {
		stream.open(directory + "index.html");
	}
	else {
		stream.open(directory + filename);
	}
	if (!stream.is_open()) {
		return false;
	}
	while (!stream.eof()) {
		std::getline(stream, line);
		m_body += line;
		m_body += "\r\n";
	}
	stream.close();
	return true;
}

void WebServer::UpdateHeader(const int &errCode) {
	std::stringstream ss;
	ss << "HTTP/1.1 ";
	switch (errCode) {
	case 200:
		ss << "200 OK";
		break;
	case 404:
		ss << "404 Not Found";
		break;
	case 403:
		ss << "403 Forbidden";
		break;
	default:
		break;
	}
	ss << "\r\nServer: The Bulldawg Server v0.2 \r\nContent-Length: ";
	ss << m_body.length();
	ss << "\r\nContent-Type: text/html; charset=utf-8\r\n\n";
	std::string tmp;
	m_header = "";
	while (!ss.eof()) { // Doing this through continous ss >> header caused a memory leak apparently
		std::getline(ss, tmp);
		m_header += tmp;
		m_header += '\n'; // For the people checking the source code in the browsers and not getting linebreaks.
	}
}

void WebServer::Print_client_address(const struct sockaddr_in *addr) {
	std::cout << (int)addr->sin_addr.S_un.S_un_b.s_b1 << ".";
	std::cout << (int)addr->sin_addr.S_un.S_un_b.s_b2 << ".";
	std::cout << (int)addr->sin_addr.S_un.S_un_b.s_b3 << ".";
	std::cout << (int)addr->sin_addr.S_un.S_un_b.s_b4;
}

void WebServer::Add_client(SOCKET socket, struct sockaddr_in &addr) {
	for (unsigned i = 0; i < m_clientlist.size(); i++) {
		if (m_clientlist[i]->m_sock == socket) {
			return; //Client already exists
		}
	}
	m_clientlist.push_back(new Client(socket, addr, timeGetTime(), true));
	Print_client_address(&addr);
	std::cout << " Connected!" << std::endl;
}

void WebServer::RemoveClient(Client *client, const unsigned int &index) {
	if (index >= m_clientlist.size()) {
		return;
	}
	if (m_clientlist[index] == client) {
		Print_client_address(&client->m_addr);
		std::cout << " Disconnected" << std::endl;
		closesocket(client->m_sock);
		delete m_clientlist[index];
		m_clientlist.erase(m_clientlist.begin() + index);
	}
}