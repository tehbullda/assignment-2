#pragma once

#include <vector>
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")

struct Client {
	Client() {
	}
	Client(SOCKET socket, struct sockaddr_in address, unsigned int time, bool keepalive) {
		m_sock = socket;
		m_addr = address;
		m_timestamp = time;
		m_keepalive = keepalive;
	}
	SOCKET m_sock;
	struct sockaddr_in m_addr;
	unsigned int m_timestamp;
	bool m_keepalive;
};

class WebServer
{
public:
	WebServer();
	~WebServer();
	bool Initialize();
	bool Run();
	bool Client_has_data(Client client);
	void Receive_data_from_client(Client *client);
	
	bool Load_page(const std::string &filename, const std::string &directory = "../data/pages/");
	void UpdateHeader(const int &errCode = 200);

	void Print_client_address(const struct sockaddr_in* addr);
	void Add_client(SOCKET socket, struct sockaddr_in &addr);
	void RemoveClient(Client *client, const unsigned int &index);

private:
	std::vector<Client*> m_clientlist;
	WSAData m_data;
	Client *m_local;
	fd_set m_fdset;
	struct timeval m_timeval;
	std::string m_header;
	std::string m_body;
};

