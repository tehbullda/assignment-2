// Assignment 2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "WebServer.h"
//#include <vld.h>

int main(int argc, char* argv[])
{
	(void)argc;
	(void)argv;
	WebServer server;
	if (server.Initialize()) {
		bool running = true;
		while (running) {
			if (!server.Run()) {
				running = false;
			}
			if (GetForegroundWindow() == GetConsoleWindow()) { // Otherwise the server shuts down as soon as Q is pressed no matter which window is in focus
				if (GetAsyncKeyState('Q') != 0) {
					running = false;
				}
			}
		}
	}
	return 0;
}